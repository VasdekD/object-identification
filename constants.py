import os

# The number of top ORB keypoints to be kept (in ORB every keypoint's descriptor has 32 features)
ORB_KEYPOINTS = 128
# The number of top SIFT keypoints to be kept (in SIFT every keypoint's descriptor has 128 features)
SIFT_KEYPOINTS = 64
# The path to the "resources" directory where necessary files are saved
resources = os.getcwd() + "\\resources\\"
# The train, validation and test_set folder names used in image feature extraction
TRAIN_DATASET = resources + "VisDrone2019-DET-train"
TEST_DATASET = resources + "VisDrone2019-DET-test-dev"
VAL_DATASET = resources + "VisDrone2019-DET-val"
# The names of the feature files of each kind of dataset
TRAIN = resources + "train.pkl"
VAL = resources + "validation.pkl"
TEST = resources + "test_dev.pkl"
# The number of features in the full image descriptor
DESCRIPTOR_LENGTH = 13056
# The number of features in the color descriptor
COLOR_DESCRIPTOR_LENGTH = 768
# The number of features in the shape descriptor
SHAPE_DESCRIPTOR_LENGTH = ORB_KEYPOINTS * 32
# The number of features in the texture descriptor
TEXTURE_DESCRIPTOR_LENGTH = SIFT_KEYPOINTS * 128
