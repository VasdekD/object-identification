import os

import constants
import cv2 as cv
import numpy as np
import pandas as pd


def get_color_desc(image_path):
    """
    Extracts the RGB histogram of the given image
    :param image_path: The path of the image to be processed
    :return: One dimension array with 3 x 256 = 768 features representing the RGB histogram of the given image
    """
    print('Extracting color descriptor for {}'.format(image_path))
    # Loads the image from the local file system
    img = cv.imread(image_path)
    # The histograms of all RGB colors will be extracted
    color = ('b', 'g', 'r')
    # Initialize an fixed size array[3] to store the 3 different histograms
    hists = [None] * 3
    # For each RGB color
    for i, col in enumerate(color):
        # Values of every pixel and for the whole [0, 256] value range
        hist = cv.calcHist([img], [i], None, [256], [0, 256])
        # The extracted histogram is flattened to one dimension
        hist = hist.flatten()
        hists[i] = hist
    # The final vector is initialized to the 1st histogram value
    rgb_hist = hists[0]
    # All three histograms are being concatenated into one vector with 768 features the chromatic patterns or the given
    # image
    for i in range(1, len(hists)):
        rgb_hist = np.concatenate((rgb_hist, hists[i]))
    return rgb_hist


def get_shape_desc(image_path, vector_size=constants.ORB_KEYPOINTS):
    """
    Extracts a shape based descriptor using ORB, a combination of FAST and BRIEF algorithms, from a given image
    :param image_path: The path of the image to be processed
    :param vector_size: The number of keypoints to be kept for the descriptor vector
    :return: a fixed size (vector_size * 32) shape based descriptor vector of the given image
    """

    print('Extracting shape descriptor for {}'.format(image_path))

    img = cv.imread(image_path)

    # Using ORB to extract features based on the combination of FAST and BRIEF algorithms
    orb = cv.ORB_create()
    # Finding image keypoints
    kps = orb.detect(img)
    # Getting the first "vector_size" of them
    # The number of keypoints can be very large and it depends on the image
    # Sorting them by the keypoint.response value (the bigger, the more important the keypoint)
    kps = sorted(kps, key=lambda x: -x.response)[:vector_size]
    # Computing the descriptor vectors of the chosen keypoints
    kps, dsc = orb.compute(img, kps)
    # Flattening them into a one-dimension array (one single vector)
    dsc = dsc.flatten()
    # Making descriptor of same size
    # Descriptor vector size in ORB is 32
    needed_size = (vector_size * 32)
    if dsc.size < needed_size:
        # if we have less the "vector_size" descriptors then just adding zeros at the
        # end of our feature vector
        dsc = np.concatenate([dsc, np.zeros(needed_size - dsc.size)])
    return dsc


def get_texture_desc(image_path, vector_size=constants.SIFT_KEYPOINTS):
    """
    Extracts a texture based descriptor using SIFT algorithm from a given image
    :param image_path: The path of the image to be processed
    :param vector_size: The number of keypoints to be kept for the descriptor vector
    :return: a fixed size (vector_size * 128) texture based descriptor vector of the given image
    """

    print('Extracting texture descriptor for {}'.format(image_path))

    img = cv.imread(image_path)

    # Using SIFT but it could be any descriptor extraction algorithm
    alg = cv.xfeatures2d.SIFT_create()
    # Finding image keypoints
    kps = alg.detect(img)
    # Getting the first "vector_size" of them
    # The number of keypoints can be very large and it depends on the image
    # Sorting them by the keypoint.response value (the bigger, the more important the keypoint)
    kps = sorted(kps, key=lambda x: -x.response)[:vector_size]
    # Computing the descriptor vectors of the chosen keypoints
    kps, dsc = alg.compute(img, kps)
    # Flattening them into a one-dimension array (one single vector)
    dsc = dsc.flatten()
    # Making descriptor of same size
    # Descriptor vector size in SIFT is 128
    needed_size = (vector_size * 128)
    if dsc.size < needed_size:
        # if we have less the "vector_size" descriptors then just adding zeros at the
        # end of our feature vector
        dsc = np.concatenate([dsc, np.zeros(needed_size - dsc.size)])
    return dsc


def get_image_desc(image_path):
    """
    Combines color, shape and texture features into a single descriptor vector for a given image
    :return: Fixed size (13.056) descriptor vector for a given image
    """

    # Extract the descriptor vector based on color features
    color_desc = get_color_desc(image_path)
    # Extract the descriptor vector based on shape features
    shape_desc = get_shape_desc(image_path)
    # Extract the descriptor vector based on texture features
    texture_desc = get_texture_desc(image_path)
    # Concatenate all of the above vectors into the final image descriptor
    image_descriptor = np.concatenate([color_desc, shape_desc, texture_desc])
    return list(image_descriptor)


def process_dataset():
    """
    Processes each one of the train, validation and test_dev datasets and extracts the descriptor vector of each image.
    This vector amongst with the image_id is exported to a csv file
    :return: None
    """

    # The script should run for each dataset separately
    dataset_path = constants.TRAIN_DATASET
    # dataset_path = constants.VAL_DATASET
    # dataset_path = constants.TEST_DATASET

    # Parsing the file structure and obtaining the descriptor vectors of the images
    dataset = os.fsencode(dataset_path)
    # Every feature of the descriptor vector is a separate column in the csv to be created
    cols = [i for i in range(0, 13057)]
    features = pd.DataFrame(columns=cols)
    # The number of the current row of the csv
    index = 0
    # For every folder of the given dataset
    for images_folder in os.listdir(dataset):
        images = os.path.join(dataset, images_folder)
        # For every image in the images folder
        for img in os.listdir(images):
            # Initializing row value
            row = []
            # Stripping the name of the image that will be used as image_id from unnecessary characters
            image_name = str(img.replace(b'.jpg', b''))
            image_name = image_name.replace("b'", '')
            image_name = image_name.replace("'", '')
            # The path of the image for which we extract the descriptor vector
            image_path = os.path.join(images, img)
            # The fist value of the row is the image_name
            row.append(image_name)
            # Extracting image descriptor
            desc = get_image_desc(image_path.decode())
            # Every feature of the descriptor is a separate column of the current row
            for feature in desc:
                row.append(feature)
            # Appending the row to the dataframe with the features of the images
            features.loc[index] = row
            # Moving the cursor to the next column
            index += 1

    # Writing the features to a csv file in the current directory
    features.to_csv("train.csv", index=False)
    # features.to_csv("test_dev.csv", index=False)
    # features.to_csv("validation.csv", index=False)


if __name__ == '__main__':
    process_dataset()
