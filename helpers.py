import constants


def get_xy(df):
    """
    Accepts a dataframe with features and labels and return two separate dataframes one with the features and one with
    the labels
    :param df: Dataset as a pandas dataframe
    :return: x: Dataset features, y: Dataset labels
    """
    # The first DESCRIPTOR_LENGTH columns are the features and the rest are the labels
    x = df.iloc[:, :constants.DESCRIPTOR_LENGTH]
    y = df.iloc[:, constants.DESCRIPTOR_LENGTH:]
    return x, y
