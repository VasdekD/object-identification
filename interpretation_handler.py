from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
import constants
from sklearn import preprocessing
import matplotlib.pyplot as plt
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics


def local_surrogate_models(x_train, y_pred, x_test, y_test, classifier, num_of_top_features=20):
    """
    Creates a plot with the most important features considered in classification of a specific instance

    :param x_train: df with features
    :param y_pred: ndarray with the black-box model's predictions for on x_val
    :param x_test: features of validation set
    :param y_test: labels of validation set
    :param classifier: the black-box classifier
    :param num_of_top_features: The number of the most important features to be retrieved

    :return: None
    """

    # We choose an instance from the test set
    new_x_train = x_train
    new_y_train = classifier.predict(x_train)
    test_x_instance = x_test.iloc[1]
    instance_pred = y_pred[1]

    # We rescale the data
    min_max_scaler = preprocessing.MinMaxScaler()
    min_max_scaler.fit(new_x_train.append(test_x_instance))
    test_x_instance_scaled = min_max_scaler.transform(test_x_instance.values.reshape(1, test_x_instance.size))
    new_x_train_scaled = min_max_scaler.transform(new_x_train)

    # We find the train instances closer to the given instance with K-NN
    print("Finding Neighbors of Instance...")
    knnmodel = KNeighborsClassifier(n_neighbors=50, weights="distance", metric="minkowski", p=2)
    knnmodel = knnmodel.fit(new_x_train_scaled, new_y_train)
    ys = knnmodel.kneighbors(test_x_instance_scaled, n_neighbors=100, return_distance=False)
    new_x_train2 = new_x_train.iloc[ys[0]]
    new_y_train2 = new_y_train[ys[0]]

    # We build a Decision Tree explanetor
    DTexplanator = DecisionTreeClassifier(random_state=0, criterion='gini', max_depth=5)
    DTexplanator.fit(new_x_train2, new_y_train2.toarray())
    fidelityPreds = DTexplanator.predict(new_x_train2)
    print("Decision Tree Prediction for Instance: " + str(
        DTexplanator.predict(
            test_x_instance.values.reshape(1, test_x_instance.size))) + " and our model predicted: " + str(
        instance_pred) + " with fidelity: " +
          str(metrics.accuracy_score(new_y_train2, fidelityPreds)))
    print("F-Score in new data")
    print(metrics.f1_score(y_test, DTexplanator.predict(x_test), average='macro'))

    # The following lines create a list which indicates what kind of feature is any given index of the feature
    # indexes
    color_features = ["color_feature"] * constants.COLOR_DESCRIPTOR_LENGTH
    shape_features = ["shape_feature"] * constants.SHAPE_DESCRIPTOR_LENGTH
    texture_features = ["texture_feature"] * constants.TEXTURE_DESCRIPTOR_LENGTH
    feature_names = color_features + shape_features + texture_features

    # We extract the most important features of the explanator
    importances = DTexplanator.feature_importances_
    indices = np.argsort(importances)
    indices = indices.tolist()
    indices = indices[::-1][:num_of_top_features]
    features = feature_names

    # We create the plot
    fig, ax = plt.subplots()
    plt.title('Feature Importances')
    plt.barh(range(len(indices)), importances[indices], color='r', align='center')
    plt.yticks(range(len(indices)), [features[i] for i in indices])
    plt.xlabel('Relative Importance')
    plt.gca().invert_yaxis()
    plot_customizations(ax)
    plt.show()

    # Saving the plot as pdf file in the current directory
    fig.savefig("top_features.pdf")


def global_surrogate_models(x_train, y_pred, x_test, y_test, classifier, num_of_top_features=20):
    """
    Creates a plot with the most important features considered in classification

    :param x_train: df with features
    :param y_pred: ndarray with the black-box model's predictions for on x_val
    :param x_test: features of validation set
    :param y_test: labels of validation set
    :param classifier: the black-box classifier
    :param num_of_top_features: The number of the most important features to be retrieved

    :return: None
    """

    # The following lines create a list which indicates what kind of feature is any given index of the feature
    # indexes
    color_features = ["color_feature"] * constants.COLOR_DESCRIPTOR_LENGTH
    shape_features = ["shape_feature"] * constants.SHAPE_DESCRIPTOR_LENGTH
    texture_features = ["texture_feature"] * constants.TEXTURE_DESCRIPTOR_LENGTH
    feature_names = color_features + shape_features + texture_features

    print("Training Explainer...")
    new_x_train = x_train
    # The explainer must learn the way the black-box model makes its predictions
    new_y_train = classifier.predict(x_train).toarray()
    explainer = RandomForestClassifier(max_depth=20, max_features=100, class_weight="balanced")
    explainer.fit(new_x_train, new_y_train)
    # The explainer and the black-box should make similar predictions
    print("Fidelity", metrics.accuracy_score(y_pred, explainer.predict(x_test)))
    print("F-Score in new data")
    print(metrics.f1_score(y_test, explainer.predict(x_test), average='macro'))

    # We retrieve the importance of each feature as estimated by the explainer
    importances = explainer.feature_importances_
    indices = np.argsort(importances)
    # We wish to keep only the most important features
    indices = indices[:num_of_top_features]
    features = feature_names

    # We crete the plot
    fig, ax = plt.subplots()
    plt.title('Feature Importances')
    plt.barh(range(len(indices)), importances[indices], color='b', align='center')
    plt.yticks(range(len(indices)), [features[i] for i in indices])
    plt.xlabel('Relative Importance')
    plot_customizations(ax)
    plt.show()

    # Saving the plot as pdf file in the current directory
    fig.savefig("top_features.pdf")


def plot_customizations(ax):
    """

    Customizations for the plots

    """

    # Remove axes spines
    for s in ['top', 'bottom', 'left', 'right']:
        ax.spines[s].set_visible(False)

    # Remove x,y Ticks
    ax.xaxis.set_ticks_position('none')
    ax.yaxis.set_ticks_position('none')

    # Add padding between axes and labels
    ax.xaxis.set_tick_params(pad=5)
    ax.yaxis.set_tick_params(pad=10)

    # Add x,y gridlines
    ax.grid(b=True, color='grey', linestyle='-.', linewidth=0.5, alpha=0.2)
