# IMPORTING MODULES
import constants
import helpers
import interpretation_handler

# IMPORTING PACKAGES
import pandas as pd
from sklearn import metrics, preprocessing
from sklearn.ensemble import RandomForestClassifier
from time import perf_counter
from imblearn.over_sampling import RandomOverSampler, SMOTE
from imblearn.under_sampling import RandomUnderSampler
from imblearn.pipeline import make_pipeline
from skmultilearn.problem_transform import BinaryRelevance, ClassifierChain


def main():

    # A timer to evaluate performance
    t1_start = perf_counter()

    # ============= LOADING DATASETS ============= #
   
    train = pd.read_pickle(constants.TRAIN)
    train = train.set_index("0")
    x_train, y_train = helpers.get_xy(train)

    test_set = pd.read_pickle(constants.TEST)
    test_set = test_set.set_index("0")
    x_test, y_test = helpers.get_xy(test_set)

    # ============= ML CLASSIFICATION MODEL ============= #

    # Random Forest model
    model = RandomForestClassifier()

    # ============= HANDLING IMBALANCE ============= #

    # Create pipeline and include random under- and over- sampling
    #pipeline = make_pipeline(RandomUnderSampler(random_state=0), RandomOverSampler(random_state=0), model)

    # Create pipeline and include synthetic over-sampling with SMOTE
    pipeline = make_pipeline(SMOTE(random_state=0), model)

    # ============= HANDLING MULTILABEL ============= #

    # Create Binary Relevance classifier
    #classifier = BinaryRelevance(pipeline)

    # Create Chain Classifier
    classifier = ClassifierChain(pipeline)

    # ============= OBTAINING PREDICTIONS ============= #

    print("Fitting the model...")
    classifier.fit(x_train, y_train)
    print("Calculating predictions...")
    y_pred = classifier.predict(x_test)

    # ============= PRINTING STATISTICS ============= #

    print("Evaluating results...")

    # Calculating F1 micro/macro scores
    print("F-score micro:", metrics.f1_score(y_test, y_pred, average="micro"))
    print("F-score macro:", metrics.f1_score(y_test, y_pred, average="macro"))

    # Calculating Accuracy for each class separately
    y_pred = y_pred.toarray()
    for i in range(0, 10):
        print("Accuracy for class {}: ".format(i), metrics.accuracy_score(y_test.iloc[:, i], y_pred[:, i]))

    # =========== INTERPRET MODEL'S CHOICE ========== #

    # LOCAL SURROGATE MODEL
    interpretation_handler.local_surrogate_models(x_train, y_pred, x_test, y_test, classifier)

    # GLOBAL SURROGATE MODEL
    interpretation_handler.global_surrogate_models(x_train, y_pred, x_test, y_test, classifier)

    # ========== CALCULATING EXECUTION TIME ========= #

    t1_stop = perf_counter()
    print("Elapsed time:", t1_stop - t1_start)


if __name__ == '__main__':
    main()
